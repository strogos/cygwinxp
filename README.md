# CYGWIN for XP

...a script which replicates the last Cygwin repository compatible with Windows XP.

- Created based on information and support of the "cygwin timemachine" collection [http://ctm.crouchingtigerhiddenfruitbat.org/Cygwin/timemachine.html](http://ctm.crouchingtigerhiddenfruitbat.org/Cygwin/timemachine.html)

- script will pull the required setup.exe, setup.ini file and cygwin mirror list from the right places. Then it will retrieve the latest version of packages from mirrors picked randomly so it would not overload a single site. There are 2 preferred mirrors with probably outdated packages and one main location where are all packages with right version (on slow link). Once done retrieving all files, then it will compare the sizes of the files with the content of setup.ini to do a simple validation. To download also the sources, you must remove the "A" from all occurrences of "^Asource".

- script can be interrupted and restarted later, it will not re-retrieve packages already downloaded.

- can be used to install Cygwin from disk or create your own local "mirror".